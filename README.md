# Hollow knight type demo platformer
#### Phaser 3 + ES6 + Webpack


**Live demo:** https://hollow-green-jade-s24ra.ondigitalocean.app//

**Source code** https://geros@bitbucket.org/geros/greenjade-hollow.git

## Controls
**Left arrow**: Moves left
**Right arrow**: Moves right
**Space**: Jumps
**Double Space**: Double jumps
**Shift** + left / right : Sprints


A messy list of things I used from the Phaser API. I'll try to improve this, but it gives a hint of what you might expect to find in the source code to read bring to your own projects.

**Preloader**
- image, tilemapTiledJSON, spritesheet, atlas, audio, audiosprite, bitmapFont, plugin

**Input**
- Phaser.Input.Keyboard
- Touch controls

**Audio**
- Audioatlas (including some event listeners)
- Music (pause/resume/rate)

**Animations**
- Animating sprites

**Tilemaps**
- Multiple layers
- Dynamic layers
- Animated tiles (Plugin: https://github.com/nkholski/phaser-animated-tiles)
- Object layers are used to manipulate the map, define areas and add enemies.

**Tilesprite**
- Background clouds

**Sprites**
- All sprites are ES6 extensions of native Phaser.Sprite

**Physics**
- Acceleration
- body sizes
- pause
- collisions and overlap sprite/sprite and sprite/tilemaplayer

**Groups**
- Sprites are put in groups

**BitmapText**
- For score and timer

**Tweens**
- entering pipes, ending the world etc.
d at Beta 19.

# Setup
You’ll need to install a few things before you have a working copy of the project.

## 1. Clone this repo:

Navigate into your workspace directory.

Run:

```git clone https://geros@bitbucket.org/geros/greenjade-hollow.git```

## 2. Install node.js and npm:

https://nodejs.org/en/


## 3. Install dependencies (optionally you could install [yarn](https://yarnpkg.com/)):

Navigate to the cloned repo’s directory.

Run:

```npm install```

or if you choose yarn, just run ```yarn```

## 4. Run the development server:

Run:

```npm run dev```

This will run a server so you can run the game in a browser.

Open your browser and enter localhost:3000 into the address bar.

Also this will start a watch process, so you can change the source and the process will recompile and refresh the browser.


## Build for deployment:

Run:

```npm run deploy```

This will optimize and minimize the compiled bundle.
