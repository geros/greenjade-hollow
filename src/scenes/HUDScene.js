import HUD from '../UI/HUD';
import eventsEmitter from '../helpers/EventEmitter';

class HUDScene extends Phaser.Scene {
	constructor() {
		super({
			key: 'HUDScene'
		});
	}

	preload() {
	}

	create() {
		// save score in registry
		this.registry.set('score', 0);
		// create HUD
		this.HUD = new HUD(this.scene);

		// create event listener for geo / coin collection from gamescene
		eventsEmitter.on('geo-collect', this.updateScore.bind(this));
		eventsEmitter.on('game-started', this.resetInventory.bind(this));
	}

	update() {}

	updateScore(value) {
		this.HUD.updateScore(value);
	}
	resetInventory() {
		this.HUD.resetInventory();
	}
}

export default HUDScene;
