import Knight from '../characters/Knight';
import {
	createGeos, createUpAndDownPlatforms, createMosscreeps
} from '../helpers/createObjs';
import constants from '../helpers/constants';
import eventsEmitter from '../helpers/EventEmitter';

class GameScene extends Phaser.Scene {
	constructor() {
		super({
			key: 'GameScene'
		});
	}

	preload() { }

	create() {
		// add bounds to world
		this.physics.world.setBounds(0, 0, constants.MAP.TILE_SIZE * constants.MAP.TILES_X, 1080);

		// signal tha gamescene started
		eventsEmitter.emit('game-started');

		// Add the map + bind the tileset
		this.map = this.make.tilemap({
			key: 'level1'
		});

		// Floor layer
		const tileset = this.map.addTilesetImage(
			'hollow_architecture',
			'Hollowtiles',
			constants.MAP.TILE_SIZE,
			constants.MAP.TILE_SIZE
		);
		this.groundLayer = this.map.createLayer('floor', tileset, 0, 0);
		this.groundLayer.y = this.cameras.main.height - this.groundLayer.height;
		this.groundLayer.setCollisionByExclusion(-1, true);
		window.ground = this.groundLayer;

		// spikes layer
		this.floorspikesLayer = this.map.createLayer('spikes_foreground', tileset, 0, 0);
		this.floorspikesLayer.y = this.cameras.main.height - this.floorspikesLayer.height;
		this.floorspikesLayer.setCollisionByExclusion(-1, true);

		// roof spikes layer
		this.roofSpikesLayer = this.map.createLayer('roof_spikes', tileset, 0, 0);
		this.roofSpikesLayer.y = this.cameras.main.height - this.roofSpikesLayer.height;
		this.roofSpikesLayer.setCollisionByExclusion(-1, true);

		// add collectible geos from tilemap andd add them to group
		this.geosGroup = this.add.group();
		createGeos(this, this.map.getObjectLayer('geosobj').objects, this.geosGroup);

		// add group holding platforms going up and down
		this.platformUpDownGroup = this.add.group();
		createUpAndDownPlatforms(
			this,
			this.map.getObjectLayer('platform_up_down').objects,
			this.platformUpDownGroup
		);
		// create Knight
		this.knight = new Knight({
			scene: this,
			key: 'knight-sprites',
			frame: 'knight_idle/Idle_000.png',
			x: 0,
			y: this.cameras.main.height - 10 * constants.MAP.TILE_SIZE
		});
		this.knight.setMapCollitions([this.groundLayer, this.geosLayer]);
		this.knight.setCoinCollitions(this.geosGroup);
		this.knight.setPlatformCollitions(this.platformUpDownGroup);
		this.knight.setSpikeCollitions([this.floorspikesLayer, this.roofSpikesLayer]);
		window.knight = this.knight;

		// add group holdingmosscreep enemies
		createMosscreeps(
			this,
			this.map.getObjectLayer('mosscreep').objects,
			[this.groundLayer, this.platformUpDownGroup],
			this.knight
		);
		// The camera should follow Knight
		this.cameras.main.setBounds(
			0, 0,
			constants.MAP.TILE_SIZE * constants.MAP.TILES_X, 1080
		);
		this.cameras.main.startFollow(this.knight, true);
		window.c = this.cameras.main;
	}

	update() {
		this.knight.update();
	}
}

export default GameScene;
