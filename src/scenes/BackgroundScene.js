
class BackgroundScene extends Phaser.Scene {
	constructor() {
		super({
			key: 'BackgroundScene',
			active: true
		});
	}

	preload() {}

	create() {
		// add background
		this.add.image(0, 0, 'background').setOrigin(0, 0);
	}

	update() {}
}

export default BackgroundScene;
