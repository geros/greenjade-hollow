import makeKnightAnimations from '../helpers/animations/knight-animations';
import makeGeoAnimations from '../helpers/animations/geo-animations';
import makeMosscreepAnimations from '../helpers/animations/mosscreep-animations';

class BootScene extends Phaser.Scene {
	constructor() {
		super({
			key: 'BootScene'
		});
	}
	preload() {
		const progress = this.add.graphics();
		// Register a load progress event to show a load bar
		this.load.on('progress', (value) => {
			progress.clear();
			progress.fillStyle(0xffffff, 1);
			progress.fillRect(
				0,
				this.sys.game.config.height / 2,
				this.sys.game.config.width * value, 60
			);
		});

		// Register a load complete event to launch the title screen when all files are loaded
		this.load.on('complete', () => {
			// prepare all animations, defined in a separate file
			makeKnightAnimations(this);
			makeGeoAnimations(this);
			makeMosscreepAnimations(this);

			progress.destroy();
			this.scene
				.launch('BackgroundScene')
				.launch('HUDScene')
				.launch('GameScene');
		});

		// add background
		// this.load.image('background', 'assets/bg.jpg');
		this.load.image('background', 'assets/cd_room_BG_01.png');

		// Tilemap with a lot of objects and tile-properties tricks
		this.load.image('Hollowtiles', 'assets/tilemaps/hollow_architecture.png');
		this.load.tilemapTiledJSON('level1', 'assets/tilemaps/level1.json');

		// sprites
		this.load.atlas('knight-sprites', 'assets/knight.png', 'assets/knight.json');
		this.load.atlas('hollow-sprites', 'assets/hollow-sprites.png', 'assets/hollow-sprites.json');
		this.load.atlas('mosscreep-sprites', 'assets/mosscreep.png', 'assets/mosscreep.json');
		// hud
		this.load.atlas('hud', 'assets/hud.png', 'assets/hud.json');

		// fonts
		this.load.bitmapFont('trajan_pro', 'assets/fonts/trajan_pro.png', 'assets/fonts/trajan_pro.fnt');
	}
}

export default BootScene;
