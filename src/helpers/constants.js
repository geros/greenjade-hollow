function constants() {
	return {
		// coin value
		MAP: {
			TILE_SIZE: 32,
			TILES_X: 91,
			TILES_Y: 40
		},
		COIN: {
			GEO_VALUE: 1,
			COIN_COUNTER_Y: 223
		},
		KNIGHT: {
			MAX_VELOCITY_X: 400,
			MAX_VELOCITY_Y: 1000,
			SPRINT_SPEED_X: 800,
			MAX_JUMPS: 2,
			ACCELERATION: 100000
		},
		MOSSCREEP: {
			ACCELERATION: 500,
			MAX_VELOCITY_X: 150
		}
	};
}
export default constants();
