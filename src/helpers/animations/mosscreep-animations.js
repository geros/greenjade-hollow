export default function makeMosscreepAnimations(scene) {
	let config = {
		key: 'mosscreep_appear',
		frames: scene.anims.generateFrameNames('mosscreep-sprites', {
			prefix: 'mosscreep_appear/Appear_',
			start: 0,
			end: 4,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 10,
		repeat: 0,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'mosscreep_bury',
		frames: scene.anims.generateFrameNames('mosscreep-sprites', {
			prefix: 'mosscreep_bury/Bury_',
			start: 0,
			end: 4,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 12,
		repeat: 0,
		repeatDelay: 0
	};
	scene.anims.create(config);
	// sprint
	config = {
		key: 'mosscreep_death',
		frames: scene.anims.generateFrameNames('mosscreep-sprites', {
			prefix: 'mosscreep_death/Death Air_',
			start: 0,
			end: 5,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 20,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'mosscreep_shake',
		frames: scene.anims.generateFrameNames('mosscreep-sprites', {
			prefix: 'mosscreep_shake/Shake_',
			start: 0,
			end: 2,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 16,
		repeat: 4,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'mosscreep_turn',
		frames: scene.anims.generateFrameNames('mosscreep-sprites', {
			prefix: 'mosscreep_turn/Turn_',
			start: 0,
			end: 2,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 12,
		repeat: 0,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'mosscreep_walk',
		frames: scene.anims.generateFrameNames('mosscreep-sprites', {
			prefix: 'mosscreep_walk/Walk_',
			start: 0,
			end: 2,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 12,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);
}
