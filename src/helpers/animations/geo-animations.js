export default function makeGeoAnimations(scene) {
	let config = {

		key: 'geo_idle',
		frames: scene.anims.generateFrameNames('hollow-sprites', {
			prefix: 'Small Idle_',
			start: 0,
			end: 7,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 12,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'geo_collect',
		frames: scene.anims.generateFrameNames('hollow-sprites', {
			prefix: 'geo_collect/Get_',
			start: 0,
			end: 3,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 25,
		repeat: 0,
		repeatDelay: 0
	};
	scene.anims.create(config);
}
