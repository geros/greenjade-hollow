export default function makeKnightAnimations(scene) {
	let config = {
		key: 'knight_idle',
		frames: scene.anims.generateFrameNames('knight-sprites', {
			prefix: 'knight_idle/Idle_',
			start: 0,
			end: 8,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 10,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'knight_walk',
		frames: scene.anims.generateFrameNames('knight-sprites', {
			prefix: 'knight_walk/Walk_',
			start: 0,
			end: 5,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 20,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);
	// sprint
	config = {
		key: 'knight_sprint',
		frames: scene.anims.generateFrameNames('knight-sprites', {
			prefix: 'knight_sprint/Sprint_',
			start: 0,
			end: 9,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 20,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'knight_doublejump',
		frames: scene.anims.generateFrameNames('knight-sprites', {
			prefix: 'knight_doublejump/Double_Jump_',
			start: 0,
			end: 7,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 16,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'knight_jump',
		frames: scene.anims.generateFrameNames('knight-sprites', {
			prefix: 'knight_jump/Airborne_jump_',
			start: 1,
			end: 6,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 12,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'knight_fall',
		frames: scene.anims.generateFrameNames('knight-sprites', {
			prefix: 'knight_fall/Airborne_fall_',
			start: 1,
			end: 6,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 12,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'knight_death',
		frames: scene.anims.generateFrameNames('knight-sprites', {
			prefix: 'knight_death/Death_',
			start: 0,
			end: 17,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 10,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);

	config = {
		key: 'knight_run_to_idle',
		frames: scene.anims.generateFrameNames('knight-sprites', {
			prefix: 'knight_death/Death_',
			start: 0,
			end: 5,
			zeroPad: 3,
			suffix: '.png'
		}),
		frameRate: 24,
		repeat: -1,
		repeatDelay: 0
	};
	scene.anims.create(config);
}
