import Geo from '../props/Geo';
import UpDownPlatform from '../props/UpDownPlatform';
import constants from './constants';
import Mosscreep from '../characters/MossCreep';

export function createGeos(scene, objects, group) {
	objects.forEach(obj => {
		let geo = new Geo({
			scene: scene,
			key: 'hollow-sprites',
			frame: 'Small Idle_000.png',
			x: obj.x,
			y: 1080 - (constants.MAP.TILES_Y * constants.MAP.TILE_SIZE - obj.y + 15)
		});
		group.add(geo);
	});
}

export function createUpAndDownPlatforms(scene, objects, group) {
	objects.forEach(obj => {
		let platform = new UpDownPlatform({
			scene: scene,
			key: 'hollow-sprites',
			frame: 'plat_float_05.png',
			x: obj.x,
			y: 1080 - (constants.MAP.TILES_Y * constants.MAP.TILE_SIZE - obj.y),
			delay: Phaser.Math.Between(0, 3000)
		});
		group.add(platform);
	});
}

export function createMosscreeps(scene, objects, collidingLayers, knight) {
	objects.forEach(obj => {
		let mosscreep = new Mosscreep({
			scene: scene,
			key: 'mosscreep-sprites',
			frame: 'mosscreep_appear/Appear_000.png',
			x: obj.x,
			y: 1080 - (constants.MAP.TILES_Y * constants.MAP.TILE_SIZE - obj.y),
			knight: knight,
			collidingLayers: collidingLayers
		});
	});
}
