import 'phaser';
import BackgroundScene from './scenes/BackgroundScene';
import BootScene from './scenes/BootScene';
import GameScene from './scenes/GameScene';
import HUDScene from './scenes/HUDScene';

const config = {
	type: Phaser.WEBGL,
	pixelArt: true,
	roundPixels: true,
	parent: 'content',
	width: 1920,
	height: 1080,
	scale: {
		mode: Phaser.Scale.FIT
	},
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 1900
			},
			debug: false
		}
	},
	fps: {
		target: 60,
		forceSetTimeOut: true
	},
	scene: [
		BootScene,
		BackgroundScene,
		HUDScene,
		GameScene
	]
};

window.addEventListener('load', () => {
	let game = new Phaser.Game(config);
});
