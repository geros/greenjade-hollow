import eventsEmitter from '../helpers/EventEmitter';
import constants from '../helpers/constants';

export default class Geo extends Phaser.GameObjects.Sprite {
	constructor(config) {
		super(config.scene, config.x, config.y, config.key, config.frame);
		config.scene.physics.world.enable(this);
		config.scene.add.existing(this);

		this.body.allowGravity = false;

		this.anims.play('geo_idle');
		this.on('animationcomplete-geo_collect', this.destroy);

		this.isDead = false;
	}

	collect() {
		if (!this.isDead) {
			this.isDead = true;
			// emit event to update the score
			eventsEmitter.emit('geo-collect', constants.COIN.GEO_VALUE);

			this.body.moves = false;
			// bring sprit in front of knight
			this.setDepth(1000);
			if (!(this.anims.isPlaying && this.anims.currentAnim.key === 'geo_collect')) {
				this.anims.play('geo_collect', false);
			}
		}
	}
}
