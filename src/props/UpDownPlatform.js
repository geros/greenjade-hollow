
export default class UpDownPlatform extends Phaser.GameObjects.Sprite {
	constructor(config) {
		super(config.scene, config.x, config.y, config.key, config.frame, config.delay);
		config.scene.add.existing(this);
		config.scene.physics.world.enable(this);

		this.setOrigin(0, 0);

		this.body.setFriction(1, 0, Infinity);
		this.body.allowGravity = false;
		this.body.immovable = true;
		this.isStatic = true;

		config.scene.tweens.add({
			targets: this,
			y: {
				from: this.y, to: this.y - 200
			},
			duration: 2000,
			ease: Phaser.Math.Easing.Sine.InOut,
			yoyo: true,
			repeat: -1,
			delay: config.delay
		});
	}
}
