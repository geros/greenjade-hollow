import constants from '../helpers/constants';

export default class InevntoryCoin extends Phaser.GameObjects.Group {
	constructor(scene) {
		super(scene);

		// coin counter
		constants.COIN.COIN_COUNTER_Y = 223;

		this.valueToBeAdded = 0;

		this.coinIcon = scene.scene.add.sprite(290, 188, 'hud', 'hud/InventoryCoin.png');
		this.add(this.coinIcon);

		// get score from registry
		const score = scene.scene.registry.get('score');
		this.scoreText = scene.scene.add.bitmapText(324, 173, 'trajan_pro', score, 44);
		this.scoreText.setCharacterTint(0, -1, true, 0xffffff);
		this.add(this.scoreText);

		this.coinCounter = scene.scene.add.bitmapText(
			324, constants.COIN.COIN_COUNTER_Y, 'trajan_pro', '0', 30
		);
		this.coinCounter.setCharacterTint(0, -1, true, 0xffffff);
		this.coinCounter.alpha = 0;
		this.add(this.coinCounter);

		// add coin counter tweens
		this.addCoinTween = this.scene.scene.tweens.add({
			targets: this.coinCounter,
			alpha: {
				from: 0, to: 1
			},
			tint: 0xffffff,
			ease: 'Linear',
			duration: 800,
			repeat: 0,
			paused: true
		});

		this.disapearCounterTeen = this.scene.scene.tweens.add({
			targets: this.coinCounter,
			alpha: {
				from: 1, to: 0
			},
			tint: 0xffffff,
			y: constants.COIN.COIN_COUNTER_Y - 30,
			ease: 'Linear',
			duration: 1000,
			repeat: 0,
			paused: true
		});

		this.addCoinTween.on('complete', () => {
			let score = this.scene.scene.registry.get('score');
			score += this.valueToBeAdded;
			this.scene.scene.registry.set('score', score);
			this.scoreText.setText(score);
			this.scoreText.setCharacterTint(0, -1, true, 0xffffff);

			this.valueToBeAdded = 0;
			this.coinCounter.setText(' 0');
			this.disapearCounterTeen.play();
		});
	}

	updateScore(value) {
		if (!this.addCoinTween.isPlaying()) {
			this.addCoinTween.play();
		} else {
			this.addCoinTween.restart();
		}
		this.valueToBeAdded += value;

		this.coinCounter.y = constants.COIN.COIN_COUNTER_Y;
		this.coinCounter.setText(`+${this.valueToBeAdded}`);
		this.coinCounter.setCharacterTint(0, -1, true, 0xffffff);
	}

	reset() {
		this.scene.scene.registry.set('score', 0);
		this.scoreText.setText(0);
		this.scoreText.setCharacterTint(0, -1, true, 0xffffff);
	}
}
