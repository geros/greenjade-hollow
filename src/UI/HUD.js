import InventoryCoin from './InventoryCoin';

export default class HUD extends Phaser.GameObjects.Group {
	constructor(scene) {
		super(scene);

		// health frame
		this.health_frame = scene.scene.add.sprite(
			115, 55, 'hud', 'hud/select_game_HUD_0002_health_frame.png'
		).setOrigin(0, 0);
		this.health_frame.scale = 1.5;
		this.add(this.health_frame);

		this.inventoryCoin = new InventoryCoin(scene);
	}

	updateScore(value) {
		this.inventoryCoin.updateScore(value);
	}

	resetInventory() {
		this.inventoryCoin.reset();
	}
}
