import * as Machina from 'machina';
import constants from '../helpers/constants';

export default class Knight extends Phaser.GameObjects.Sprite {
	constructor(config) {
		super(config.scene, config.x, config.y, config.key, config.frame);
		config.scene.physics.world.enable(this);
		config.scene.add.existing(this);

		this.body.setSize(65, 94);
		this.body.offset.y += 20;

		this.body.maxVelocity.x = constants.KNIGHT.MAX_VELOCITY_X;
		this.body.maxVelocity.y = constants.KNIGHT.MAX_VELOCITY_Y;
		this.body.setVelocity(0, 0);
		this.body.setCollideWorldBounds(true);

		// state variables
		this.isRunning = false;
		this.numberOfJumps = 0;
		this.isWalking = {
			left: false,
			right: false
		};
		this.isOnPlatform = false;

		// this.keys will contain all we need to control Knight.
		// Any key could just replace the default (like this.key.jump)
		this.keys = {
			left: this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT),
			right: this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT),
			jump: this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE),
			shift: this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SHIFT)
		};

		// create FSM
		this.fsm = new Machina.Fsm({
			namespace: 'knight',
			initialState: 'Idle',
			states: {
				Idle: {
					_onEnter: () => {
						console.log('Idle State');
						this.body.setVelocity(0);
						this.body.setAcceleration(0);
						this.anims.play('knight_idle');

						// state variables
						this.isWalking = {
							left: false,
							right: false
						};
						this.numberOfJumps = 0;
					},
					handleButtons: inputs => {
						if (inputs.left.isDown || inputs.right.isDown) {
							this.fsm.transition('Walk');
						}
					},
					handleYvelocity: () => {
						if (this.body.velocity.y > 0 && !this.isOnPlatform) {
							this.fsm.transition('Falling');
						}
					},
					handleJumpPressed: () => {
						this.fsm.transition('Jump');
					},
					handlePlatformCollition: platform => {
						// nasty hack
						this.body.velocity.y = platform.body.velocity.y + 400;
					},
					mosscreepCollition: mossCreepFSMState => {
						if (mossCreepFSMState === 'Walk') {
							this.fsm.transition('Death');
						}
					},
					spikeCollition: () => {
						this.fsm.transition('Death');
					}
				},
				Falling: {
					_onEnter: () => {
						console.log('Falling State');
						this.anims.play('knight_fall');

						// state variables
						this.isWalking = {
							left: false,
							right: false
						};
					},
					handleButtons: inputs => {
						const acceleration = constants.KNIGHT.ACCELERATION;
						if (inputs.left.isDown) {
							this.flipX = false;
							this.body.setAccelerationX(-acceleration);
						}
						if (inputs.right.isDown) {
							this.flipX = true;
							this.body.setAccelerationX(acceleration);
						}
					},
					touchedTileBelow: () => {
						this.fsm.transition('Idle');
					},
					handleJumpPressed: () => {
						if (this.numberOfJumps < constants.KNIGHT.MAX_JUMPS) {
							this.fsm.transition('DoubleJump');
						}
					},
					handlePlatformCollition: platform => {
						this.fsm.transition('Idle');
					},
					mosscreepCollition: mossCreepFSMState => {
						if (mossCreepFSMState === 'Walk') {
							this.fsm.transition('Death');
						}
					},
					spikeCollition: () => {
						this.fsm.transition('Death');
					}
				},
				Walk: {
					_onEnter: () => {
						console.log('Walk State');
						// state variable
						this.isRunning = false;
						// animation
						if (!(this.anims.isPlaying && this.anims.currentAnim.key === 'knight_walk')) {
							this.anims.play('knight_walk', true);
						}
					},
					handleButtons: inputs => {
						if (inputs.shift.isDown) {
							this.isRunning = true;
							this.fsm.transition('Sprint');
						}
						if (inputs.left.isDown) {
							this.flipX = false;
							this.isWalking.left = true;
							this.isWalking.right = false;
							this.walk('left', this.isRunning);
						}
						if (inputs.right.isDown) {
							this.flipX = true;
							this.isWalking.left = false;
							this.isWalking.right = true;
							this.walk('right', this.isRunning);
						}
						if (inputs.left.isUp && inputs.right.isUp) {
							this.fsm.transition('Idle');
						}
					},
					handleYvelocity: () => {
						if (this.body.velocity.y > 0 && !this.isOnPlatform) {
							this.fsm.transition('Falling');
						}
					},
					handleJumpPressed: () => {
						this.fsm.transition('Jump');
					},
					mosscreepCollition: mossCreepFSMState => {
						if (mossCreepFSMState === 'Walk') {
							this.fsm.transition('Death');
						}
					},
					spikeCollition: () => {
						this.fsm.transition('Death');
					}
				},
				Sprint: {
					_onEnter: () => {
						console.log('Sprint State');
						// state variable
						// animation
						if (!(this.anims.isPlaying && this.anims.currentAnim.key === 'knight_sprint')) {
							this.anims.play('knight_sprint', true);
						}
					},
					handleButtons: inputs => {
						if (inputs.shift.isUp) {
							this.isRunning = false;
							this.fsm.transition('Walk');
						}
						if (inputs.left.isDown) {
							this.flipX = false;
							this.isWalking.left = true;
							this.isWalking.right = false;
							this.walk('left', this.isRunning);
						}
						if (inputs.right.isDown) {
							this.flipX = true;
							this.isWalking.left = false;
							this.isWalking.right = true;
							this.walk('right', this.isRunning);
						}
						if (inputs.left.isUp && inputs.right.isUp) {
							this.anims.play('knight_run_to_idle');
							this.fsm.transition('Idle');
						}
					},
					handleJumpPressed: () => {
						this.fsm.transition('Jump');
					},
					mosscreepCollition: mossCreepFSMState => {
						if (mossCreepFSMState === 'Walk') {
							this.fsm.transition('Death');
						}
					},
					spikeCollition: () => {
						this.fsm.transition('Death');
					}
				},
				Jump: {
					_onEnter: () => {
						console.log('Jump State');
						// animation
						if (!(this.anims.isPlaying && this.anims.currentAnim.key === 'knight_jump')) {
							this.anims.play('knight_jump', false);
						}

						// state variables
						this.isOnPlatform = false;
						this.numberOfJumps += 1;
						this.isWalking = {
							left: false,
							right: false
						};

						this.body.setVelocityY(-this.body.maxVelocity.y);
					},
					handleButtons: inputs => {
						const acceleration = constants.KNIGHT.ACCELERATION;
						if (inputs.left.isDown) {
							this.flipX = false;
							this.body.setAccelerationX(-acceleration);
						} else {
							this.body.setAccelerationX(0);
						}
						if (inputs.right.isDown) {
							this.flipX = true;
							this.body.setAccelerationX(acceleration);
						} else {
							this.body.setAccelerationX(0);
						}
					},
					touchedTileBelow: () => {
						this.fsm.transition('Idle');
					},
					handleYvelocity: () => {
						if (this.body.velocity.y > 0 && !this.isOnPlatform) {
							this.fsm.transition('Falling');
						}
					},
					handleJumpPressed: () => {
						this.fsm.transition('DoubleJump');
					},
					handleJumpReleased: () => {
						this.body.setVelocityY(0);
					},
					mosscreepCollition: mossCreepFSMState => {
						if (mossCreepFSMState === 'Walk') {
							this.fsm.transition('Death');
						}
					},
					spikeCollition: () => {
						this.fsm.transition('Death');
					}
				},
				DoubleJump: {
					_onEnter: () => {
						console.log('DoubleJump State');
						// animation
						if (!(this.anims.isPlaying && this.anims.currentAnim.key === 'knight_doublejump')) {
							this.anims.play('knight_doublejump', false);
						}

						// state variables
						this.isOnPlatform = false;
						this.numberOfJumps += 1;
						this.isWalking = {
							left: false,
							right: false
						};

						this.body.setVelocityY(-this.body.maxVelocity.y);
					},
					handleButtons: inputs => {
						const acceleration = constants.KNIGHT.ACCELERATION;
						if (inputs.left.isDown) {
							this.flipX = false;
							this.body.setAccelerationX(-acceleration);
						}
						if (inputs.right.isDown) {
							this.flipX = true;
							this.body.setAccelerationX(acceleration);
						}
					},
					touchedTileBelow: () => {
						this.fsm.transition('Idle');
					},
					handleYvelocity: () => {
						if (this.body.velocity.y > 0 && !this.isOnPlatform) {
							this.fsm.transition('Falling');
						}
					},
					handlePlatformCollition: () => {
						this.fsm.transition('Idle');
					},
					spikeCollition: () => {
						this.fsm.transition('Death');
					},
					mosscreepCollition: mossCreepFSMState => {
						if (mossCreepFSMState === 'Walk') {
							this.fsm.transition('Death');
						}
					}
				},
				Death: {
					_onEnter: () => {
						console.log('Death State');

						this.body.setVelocity(0);
						this.body.setAcceleration(0);
						this.body.enable = false;
						this.anims.play('knight_death', false);

						setTimeout(() => {
							this.scene.scene.restart('GameScene');
						}, 2000);
					}
				}
			},
			handleInputs: inputs => {
				this.fsm.handle('handleButtons', inputs);
				this.fsm.handle('handleYvelocity');
			},
			handleTileCollition: () => {
				if (this.body.blocked.down) {
					this.fsm.handle('touchedTileBelow');
				}
			},
			handlePlatformCollition: platform => {
				this.isOnPlatform = true;
				this.fsm.handle('handlePlatformCollition', platform);
			},
			handleSpikeCollition: () => {
				this.fsm.handle('spikeCollition');
			},
			handleJumpPressed: () => {
				this.fsm.handle('handleJumpPressed');
			},
			handleJumpReleased: () => {
				this.fsm.handle('handleJumpReleased');
			},
			handleMosscreepCollition: mossCreepFSMState => {
				this.fsm.handle('mosscreepCollition', mossCreepFSMState);
			}
		});

		// register key events
		this.keys.jump.on('down', () => {
			this.fsm.handleJumpPressed();
		});
		this.keys.jump.on('up', () => {
			this.fsm.handleJumpReleased();
		});
	}

	// floor collitions
	setMapCollitions(layers) {
		layers.forEach(layer => {
			this.scene.physics.add.collider(this, layer, this.floorCollition);
		});
	}

	floorCollition(knight) {
		knight.fsm.handleTileCollition();
	}

	// geo / coin collitions
	setCoinCollitions(geosGroup) {
		this.scene.physics.add.collider(this, geosGroup, this.coinCollition);
	}

	coinCollition(knight, coin) {
		coin.collect();
	}

	// platform collition
	setPlatformCollitions(platformsGroup) {
		this.scene.physics.add.collider(this, platformsGroup, this.platformCollition);
	}

	platformCollition(knight, platform) {
		knight.fsm.handlePlatformCollition(platform);
	}

	// platform collition
	setSpikeCollitions(skikeLayers) {
		skikeLayers.forEach(layer => {
			this.scene.physics.add.collider(this, layer, this.spikeCollition);
		});
	}

	spikeCollition(knight, spike) {
		console.log('spike collition');
		knight.fsm.handleSpikeCollition(spike);
	}

	// moss collition
	checkMosscreepCollition(mossCreepFSMState) {
		this.fsm.handleMosscreepCollition(mossCreepFSMState);
	}

	update() {
		this.fsm.handleInputs(this.keys);
	}

	walk(direction, isSprint) {
		let acceleration = constants.KNIGHT.ACCELERATION;
		if (isSprint) {
			acceleration *= 2;
			this.body.maxVelocity.x = constants.KNIGHT.SPRINT_SPEED_X;
		} else {
			this.body.maxVelocity.x = constants.KNIGHT.MAX_VELOCITY_X;
		}
		if (direction === 'left') {
			acceleration = -acceleration;
		}
		this.body.setAccelerationX(acceleration);
	}
}
