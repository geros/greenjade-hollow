import * as Machina from 'machina';
import constants from '../helpers/constants';
import MosscreepSprite from './MossCreepSprite';

export default class Mosscreep extends Phaser.GameObjects.Group {
	constructor(config) {
		super(config.scene);

		// keep reference of knight (not sure if its a good idea)
		this.knight = config.knight;

		this.mosscreep = new MosscreepSprite(config);
		this.mosscreep.on('animationcomplete-mosscreep_shake', this.shakeAnimationEnded.bind(this));
		this.mosscreep.on('animationcomplete-mosscreep_bury', this.buryAnimationEnded.bind(this));
		this.mosscreep.on('animationcomplete-mosscreep_turn', this.turnAnimationEnded.bind(this));
		this.add(this.mosscreep);

		// variable to keep walking timeout
		this.walkingTimeout = null;
		this.isWalkingRight = true;

		this.fsm = new Machina.Fsm({
			namespace: 'mosscreep',
			initialState: 'Idle',
			states: {
				Idle: {
					_onEnter: () => {
						console.log('%c Idle State', 'background: #222; color: #ff9800');
						clearTimeout(this.walkingTimeout);
						this.walkingTimeout = null;
					},
					handleKnightOverlap: () => {
						this.fsm.transition('Shake');
					}
				},
				Appear: {
					_onEnter: () => {
						console.log('%c Appear State', 'background: #222; color: #ff9800');
						this.playAnimation('mosscreep_appear');
					}
				},
				Walk: {
					_onEnter: () => {
						console.log('%c Walk State', 'background: #222; color: #ff9800');
						this.playAnimation('mosscreep_walk');

						if (this.fsm.priorState === 'Shake') {
							this.mosscreep.y -= 20;
							if (this.mosscreep.body.x < this.knight.x) {
								this.isWalkingRight = true;
								this.walk(this.isWalkingRight);
							} else {
								this.isWalkingRight = false;
							}
							this.walk(this.isWalkingRight);
						} else if (this.fsm.priorState === 'Turn') {
							this.isWalkingRight = !this.isWalkingRight;
							this.walk(this.isWalkingRight);
						}
						if (!this.walkingTimeout) {
							this.walkingTimeout = setTimeout(() => {
								this.fsm.handleWalkingTimeOut();
							}, 5000);
						}
					},
					_onExit: () => {
						this.stop();
					},
					handleKnightOverlap: () => {
						this.stop();
						this.mosscreep.anims.pause();
					},
					tileCollition: () => {
						this.fsm.transition('Turn');
					},
					walkingTimeOut: () => {
						clearTimeout(this.walkingTimeout);
						this.walkingTimeout = null;
						this.fsm.transition('Bury');
					}
				},
				Turn: {
					_onEnter: () => {
						console.log('%c Turn State', 'background: #222; color: #ff9800');
						this.stop();
						this.playAnimation('mosscreep_turn');
					},
					turnAnimationEnded: () => {
						this.fsm.transition('Walk');
					}
				},
				Bury: {
					_onEnter: () => {
						console.log('%c Bury State', 'background: #222; color: #ff9800');
						this.playAnimation('mosscreep_bury');
					},
					buryAnimationEnded: () => {
						this.fsm.transition('Idle');
					}
				},
				Shake: {
					_onEnter: () => {
						console.log('%c Shake State', 'background: #222; color: #ff9800');
						this.playAnimation('mosscreep_shake');
					},
					shakeAnimationEnded: () => {
						console.log('handle shae');
						this.fsm.transition('Walk');
					}
				},
				Death: {
					_onEnter: () => {
						console.log('%c Death State', 'background: #222; color: #ff9800');
						this.playAnimation('mosscreep_death');
					}
				}
			},
			handleKnightOverlap: () => {
				this.fsm.handle('handleKnightOverlap');
			},
			handleshakeAnimationEnded: () => {
				this.fsm.handle('shakeAnimationEnded');
			},
			handleburyAnimationEnded: () => {
				this.fsm.handle('buryAnimationEnded');
			},
			handleturnAnimationEnded: () => {
				this.fsm.handle('turnAnimationEnded');
			},
			handleTileCollition: () => {
				this.fsm.handle('tileCollition');
			},
			handleWalkingTimeOut: () => {
				this.fsm.handle('walkingTimeOut');
			}
		});

		this.scene.physics.add.overlap(
			this, config.knight, this.handleKnightOverlap.bind(this)
		);

		// add floor coliders
		config.collidingLayers.forEach(layer => {
			config.scene.physics.add.collider(this, layer, this.floorCollition.bind(this));
		});
		window.moss = this;
	}

	floorCollition(mosscreep, tile) {
		if (mosscreep.body.blocked.left || mosscreep.body.blocked.right) {
			this.fsm.handleTileCollition();
		}
	}

	handleKnightOverlap() {
		this.knight.checkMosscreepCollition(this.fsm.state);
		this.fsm.handleKnightOverlap();
	}

	shakeAnimationEnded() {
		this.fsm.handleshakeAnimationEnded();
	}

	buryAnimationEnded() {
		this.fsm.handleburyAnimationEnded();
	}

	turnAnimationEnded() {
		this.fsm.handleturnAnimationEnded();
	}

	walk(isWalkingRight) {
		let acceleration = constants.MOSSCREEP.ACCELERATION;
		if (isWalkingRight) {
			this.mosscreep.flipX = true;
		} else {
			this.mosscreep.flipX = false;
			acceleration = -acceleration;
		}
		this.mosscreep.body.maxVelocity.x = constants.MOSSCREEP.MAX_VELOCITY_X;
		this.mosscreep.body.setAccelerationX(acceleration);
	}

	stop() {
		this.mosscreep.body.setAccelerationX(0);
		this.mosscreep.body.maxVelocity.x = 0;
	}
}
