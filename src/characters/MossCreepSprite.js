
export default class MosscreepSprite extends Phaser.GameObjects.Sprite {
	constructor(config) {
		super(config.scene, config.x, config.y, config.key, config.frame);
		config.scene.physics.world.enable(this);
		config.scene.add.existing(this);

		this.setOrigin(0, 0);

		this.body.setVelocity(0, 0);
		this.body.setCollideWorldBounds(true);
		this.body.collideWorldBounds = true;

		this.body.setSize(65, 94);
		this.body.offset.y = 15;
	}
}
